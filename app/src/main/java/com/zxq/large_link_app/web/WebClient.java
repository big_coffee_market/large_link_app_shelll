package com.zxq.large_link_app.web;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.fragment.app.FragmentActivity;
import com.github.gzuliyujiang.wheelpicker.OptionPicker;
import com.github.gzuliyujiang.wheelpicker.contract.OnOptionPickedListener;
import com.tbruyelle.rxpermissions3.RxPermissions;
import com.zxq.large_link_app.map.NavigationMap;
import io.reactivex.rxjava3.functions.Consumer;


public class WebClient extends WebViewClient  {

    private RxPermissions rxPermissions;
    private FragmentActivity fragmentActivity;


    public WebClient(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
        rxPermissions = new RxPermissions(fragmentActivity);
    }



    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        final String requestUrl = request.getUrl().toString();
        if(requestUrl.contains("tel:")) {
            rxPermissions.request(Manifest.permission.CALL_PHONE)
                    .subscribe(new Consumer<Boolean>() {
                @Override
                public void accept(Boolean aBoolean) throws Throwable {
                      if(aBoolean) {
                          Intent intent = new Intent(Intent.ACTION_DIAL);
                          Uri data = Uri.parse(requestUrl);
                          intent.setData(data);
                          fragmentActivity.startActivity(intent);
                      }
                }});
        } else {
            view.loadUrl(requestUrl);
        }
        return true;
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }



}
