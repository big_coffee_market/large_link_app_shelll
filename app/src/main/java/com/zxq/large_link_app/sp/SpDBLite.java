package com.zxq.large_link_app.sp;

import android.content.Context;
import android.content.SharedPreferences;

public class SpDBLite implements ISaveH5Url {



    public static ISaveH5Url getObj() {
        return spDBLite;
    }


    public static void config(Context context) {
        if(spDBLite == null) {
          spDBLite =  new SpDBLite(context);
        }
    }

    static SpDBLite spDBLite = null;

    SharedPreferences sp = null;

    final String h5Action = "/h5/login";
    final String jointPort = ":8080";
    final String baseUrl = "http://sf.zxqnywlw.cn";

    protected  SpDBLite(Context context) {
        sp = context.getSharedPreferences("h5Web",Context.MODE_PRIVATE);
        spDBLite = this;
    }


    @Override
    public void saveBaseUrl(String url) {
          if(url.endsWith("/")) {
              url  = url.substring(0,url.lastIndexOf("/"));
          }
          sp.edit().putString("baseUrl",url).commit();
    }

    @Override
    public String getH5Url() {
        String prefixUrl = sp.getString("baseUrl",baseUrl);
        if(prefixUrl.endsWith("/")) {
            prefixUrl  = prefixUrl.substring(0,prefixUrl.lastIndexOf("/"));
        }

        return prefixUrl + h5Action;
    }



    @Override
    public String getWebJoint() {
        String prefixUrl = sp.getString("baseUrl",baseUrl);
        if(prefixUrl.endsWith("/")) {
            prefixUrl  = prefixUrl.substring(0,prefixUrl.lastIndexOf("/"));
        }

        return prefixUrl + jointPort;
    }

}
