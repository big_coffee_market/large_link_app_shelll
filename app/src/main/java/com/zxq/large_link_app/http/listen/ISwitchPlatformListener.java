package com.zxq.large_link_app.http.listen;

public interface ISwitchPlatformListener {

    /**
     * 注册监听事件
     * @param url
     */
    void notifyPlatformSwitch(String url);

}
